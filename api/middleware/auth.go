package middleware

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/auth"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
)

// authentication check
func AuthGuard() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("authorization")

		if authHeader == "" {
			response.SendError(c, http.StatusUnauthorized, "Unauthorized: Token is missing", nil)
			c.Abort()
		}

		token := strings.Fields(authHeader)[1]

		tokenData, err := auth.VerifyAccessToken(token)
		if err != nil {
			// msg := "Unauthorized: Token is invalid"
			response.SendError(c, http.StatusUnauthorized, err.Error(), nil)
			c.Abort()
		}

		c.Set("userTokenData", tokenData)

		c.Next()

	}
}

// This middleware depends on 'AuthGuard' middleware, and thus should be used
// immediately after 'AuthGuard' middleware for correct functionality
func AdminGuard() gin.HandlerFunc {
	return func(c *gin.Context) {
		// TODO: avoid code duplication when checking for 'userTokenData' and during type assertions
		userTokenData, exists := c.Get("userTokenData")
		if exists == false { // user token data does not exist
			response.SendError(c, http.StatusInternalServerError, "an error occurred", "userTokenData is missing")
			c.Abort()
		}

		claims, ok := userTokenData.(*auth.JWTClaims) // type assertion
		if !ok {                                      // type assertion not successfully
			response.SendError(c, http.StatusInternalServerError, "an error occurred", "type asserion failed")
			c.Abort()
		}

		if claims.IsAdmin == false { // client doesn't have admin permissions
			response.SendError(c, http.StatusUnauthorized, "you do not have permissions for this action", nil)
			c.Abort()
		}

		c.Next()
	}
}

// This middleware depends on 'AuthGuard' middleware, and thus should be used
// immediately after 'AuthGuard' middleware for correct functionality.
// This middleware restricts a user from modifying/retrieving another user's 'resource'.
// Admins, however, have permissions to modify/retrieve other user's 'resources'
func OwnUserGuard() gin.HandlerFunc {
	return func(c *gin.Context) {
		// TODO: avoid code duplication when checking for 'userTokenData' and during type assertions
		userTokenData, exists := c.Get("userTokenData")
		if exists == false { // user token data does not exist
			response.SendError(c, http.StatusInternalServerError, "an error occurred", "userTokenData is missing")
			c.Abort()
		}

		claims, ok := userTokenData.(*auth.JWTClaims) // type assertion
		if !ok {                                      // type assertion not successfully
			response.SendError(c, http.StatusInternalServerError, "an error occurred", "type asserion failed")
			c.Abort()
		}

		// retrieve a user id from request param
		id := c.Param("userID")
		userID, err := strconv.Atoi(id)
		if err != nil {
			response.SendError(c, http.StatusBadRequest, "invalid user id value", err.Error())
			c.Abort()
		}

		if userID != claims.UserID && !claims.IsAdmin { // the user doesn't 'own' the resource and is not an admin
			response.SendError(c, http.StatusUnauthorized, "you do not have permission modify or retrieve another user's resource", nil)
			c.Abort()
		}

		c.Next()
	}
}
