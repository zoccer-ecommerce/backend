package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func SampleMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("THIS IS A MIDDLEWARE")
		c.Set("someKey", "someValue")

		c.Next()

	}
}
