package services

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
	"gorm.io/gorm"
)

type PlaceOrderInput struct {
	DeliveryAddress string `json:"deliveryAddress" binding:"required"`
	ContactNUmber   string `json:"contactNumber" binding:"required"`
}

// Place an order
func PlaceOrder(orderDetails models.Order) (models.Order, error) {
	order := models.Order{
		UserID:          orderDetails.UserID,
		CartID:          orderDetails.CartID,
		Status:          "pending", // TODO: store such constants in one place and reference where needed
		Subtotal:        orderDetails.Subtotal,
		Discount:        orderDetails.Discount,
		TotalAmount:     orderDetails.TotalAmount,
		DeliveryAddress: orderDetails.DeliveryAddress,
		ContactNumber:   orderDetails.ContactNumber,
	}

	err := setup.DB.Create(&order).Error
	return order, err
}

// Retrieve user orders
func FindUserOrders(userID uint, paginate func(db *gorm.DB) *gorm.DB) ([]models.Order, int64, error) {
	var orders []models.Order

	allOrdersCount := setup.DB.Where("user_id = ?", userID).Model(&models.Order{}).Find(&orders).RowsAffected
	queryBuilder := setup.DB.Where("user_id = ?", userID).Scopes(paginate).Order("id desc")
	result := queryBuilder.Where("user_id = ?", userID).Model(&models.Order{}).Find(&orders)

	return orders, allOrdersCount, result.Error
}

// Retrieve a single user's order by ID
func FindUserOrder(userID, orderID uint) (models.Order, error) {
	var order models.Order

	result := setup.DB.Where("user_id = ?", userID).Model(&models.Order{}).First(&order, orderID)
	return order, result.Error
}

// Retrieve an order's order items
func FindOrderItems(cartID uint, paginate func(db *gorm.DB) *gorm.DB) ([]models.CartItem, int64, error) {
	var orderItems []models.CartItem

	allOrderItemsCount := setup.DB.Where("cart_id = ?", cartID).Preload("Product").Preload("Product.ProductCategory").Model(&models.CartItem{}).Find(&orderItems).RowsAffected
	queryBuilder := setup.DB.Where("cart_id = ?", cartID).Preload("Product").Preload("Product.ProductCategory").Scopes(paginate).Order("id desc")
	result := queryBuilder.Where("cart_id = ?", cartID).Preload("Product").Preload("Product.ProductCategory").Model(&models.CartItem{}).Find(&orderItems)

	return orderItems, allOrderItemsCount, result.Error
}

// Retrieve a single order's order item by ID
func FindOrderItem(cartID, orderItemID uint) (models.CartItem, error) {
	var order models.CartItem

	result := setup.DB.Where("cart_id = ?", cartID).Preload("Product").Preload("Product.ProductCategory").Model(&models.CartItem{}).First(&order, orderItemID)
	return order, result.Error
}
