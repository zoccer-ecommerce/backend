package controllers

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	cartService "gitlab.com/zoccer-ecommerce/backend/api/features/cart/services"
	"gitlab.com/zoccer-ecommerce/backend/api/features/orders/services"
	usersService "gitlab.com/zoccer-ecommerce/backend/api/features/users/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/auth"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/pagination"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

func PlaceOrder(c *gin.Context) {
	// validate input
	var input services.PlaceOrderInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// first obtain the user's cart
	foundUserCart, err := cartService.FindUserCart(uint(claims.UserID))
	if err != nil {
		if err.Error() == "record not found" { // user's cart does not exist
			msg := fmt.Sprintf("user has no cart")
			response.SendError(c, http.StatusNotFound, msg, err.Error())
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving user's cart", err.Error())
		return
	}

	// retrieve the cart items
	foundCartItems, _, err := cartService.FindCartItemsWithoutPagination(foundUserCart.ID)
	if err != nil {
		if err.Error() == "record not found" { // user's cart does not exist
			msg := fmt.Sprintf("user has an empty cart")
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving user's cart items", err.Error())
		return
	}

	// calculate the total order amount after applying discount (from the cart)
	var orderAmount int
	for _, cartItem := range foundCartItems {
		orderAmount += (cartItem.Quantity * (cartItem.Product.Price - cartItem.Product.DiscountPrice))
	}

	// add record to Order table
	orderDetails := models.Order{
		UserID:          uint(claims.UserID),
		CartID:          foundUserCart.ID,
		Subtotal:        orderAmount,
		Discount:        0,
		TotalAmount:     orderAmount - 0,
		DeliveryAddress: input.DeliveryAddress,
		ContactNumber:   input.ContactNUmber,
	}

	if _, err := services.PlaceOrder(orderDetails); err != nil {
		msg := fmt.Sprintf("error placing an order")
		response.SendError(c, http.StatusInternalServerError, msg, err.Error())
		return
	}

	// change the cart's status
	detailsToUpdate := models.Cart{
		Status: "ordered",
	}
	if _, err := cartService.UpdateCart(foundUserCart, detailsToUpdate); err != nil {
		msg := fmt.Sprintf("error updating cart status")
		response.SendError(c, http.StatusInternalServerError, msg, err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "order placed successfully", nil)
}

func FindUserOrders(c *gin.Context) {
	paginationValues := pagination.GeneratePaginationValues(c)
	paginate := pagination.Paginate(c)

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// check whether the user exists
	if err, _ := usersService.FindUser(uint(claims.UserID)); err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("user with id %v does not exist", claims.UserID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving user's cart items", err.Error())
		return
	}

	// retrieve user orders
	foundOrders, ordersCount, err := services.FindUserOrders(uint(claims.UserID), paginate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error retrieving orders", err.Error())
		return
	}

	totalPages := math.Ceil(float64(ordersCount) / float64(paginationValues.PageSize))
	data := struct {
		Count  int64          `json:"count"`
		Pages  int            `json:"pages"`
		Orders []models.Order `json:"orders"`
	}{
		Count:  ordersCount,
		Pages:  int(totalPages),
		Orders: foundOrders,
	}

	response.SendSuccess(c, http.StatusOK, "orders retrieved successfully", data)
}

func FindUserOrder(c *gin.Context) {
	// retrieve orderID from request
	id := c.Param("orderID")
	orderID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer value as order id", nil)
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// retrieving the order
	foundOrder, err := services.FindUserOrder(uint(claims.UserID), uint(orderID))
	if err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("order with id %v does not exist", orderID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving order", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "order retrieved successfully", foundOrder)
}

func FindOrderItems(c *gin.Context) {
	paginationValues := pagination.GeneratePaginationValues(c)
	paginate := pagination.Paginate(c)

	// retrieve orderID from request
	id := c.Param("orderID")
	orderID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer value as order id", nil)
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// check whether the user exists
	if err, _ := usersService.FindUser(uint(claims.UserID)); err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("user with id %v does not exist", claims.UserID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving user's cart items", err.Error())
		return
	}

	// retrieve the order
	foundOrder, err := services.FindUserOrder(uint(claims.UserID), uint(orderID))
	if err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("order with id %v does not exist", orderID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving order", err.Error())
		return
	}

	// retrieving order items
	foundOrderItems, orderItemsCount, err := services.FindOrderItems(foundOrder.CartID, paginate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error retrieving order items", err.Error())
		return
	}

	totalPages := math.Ceil(float64(orderItemsCount) / float64(paginationValues.PageSize))
	data := struct {
		Count      int64             `json:"count"`
		Pages      int               `json:"pages"`
		OrderItems []models.CartItem `json:"orderItems"`
	}{
		Count:      orderItemsCount,
		Pages:      int(totalPages),
		OrderItems: foundOrderItems,
	}

	response.SendSuccess(c, http.StatusOK, "order items retrieved successfully", data)
}

func FindOrderItem(c *gin.Context) {
	// retrieve orderID from request
	id := c.Param("orderID")
	orderID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer value as order id", nil)
		return
	}

	// retrieve orderItemID from request
	id2 := c.Param("orderItemID")
	orderItemID, err := strconv.Atoi(id2)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer value as order item id", nil)
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// retrieving the order
	foundOrder, err := services.FindUserOrder(uint(claims.UserID), uint(orderID))
	if err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("order with id %v does not exist", orderID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving order", err.Error())
		return
	}

	// retrieving the order item
	foundOrderItem, err := services.FindOrderItem(foundOrder.CartID, uint(orderItemID))
	if err != nil {
		if err.Error() == "record not found" {
			msg := fmt.Sprintf("order item with id %v does not exist", orderID)
			response.SendError(c, http.StatusNotFound, msg, nil)
			return
		}

		// other errors
		response.SendError(c, http.StatusInternalServerError, "error retrieving order item", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "order item retrieved successfully", foundOrderItem)
}
