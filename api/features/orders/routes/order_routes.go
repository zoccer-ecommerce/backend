package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/orders/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)

func Orders(routesGroup *gin.RouterGroup) *gin.RouterGroup {
	ordersApi := routesGroup.Group("/orders")
	{
		ordersApi.POST("/", middleware.AuthGuard(), controllers.PlaceOrder)
		ordersApi.GET("/", middleware.AuthGuard(), controllers.FindUserOrders)
		ordersApi.GET("/:orderID", middleware.AuthGuard(), controllers.FindUserOrder)

		// order items routes
		ordersApi.GET("/:orderID/items", middleware.AuthGuard(), controllers.FindOrderItems)
		ordersApi.GET("/:orderID/items/:orderItemID", middleware.AuthGuard(), controllers.FindOrderItem)
	}

	return ordersApi
}
