package services

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

// TODO: add this struct to validations package
type CreateProductCategoryInput struct {
	Name string `json:"name" binding:"required"`
}

type UpdateProductCategoryInput struct {
	Name string `json:"name" binding:"required"`
}

// Create new Prouct
func CreateProductCategory(input CreateProductCategoryInput) (error, models.ProductCategory) {
	productCategory := models.ProductCategory{
		Name: input.Name,
	}

	err := setup.DB.Create(&productCategory).Error
	return err, productCategory
}

// Get all product categories
func FindProductCategories() (error, []models.ProductCategory) {
	var productCategories []models.ProductCategory
	err := setup.DB.Order("id desc").Model(&models.ProductCategory{}).Find(&productCategories).Error
	return err, productCategories
}

// Get a product category by id
func FindProductCategory(id uint) models.ProductCategory {
	var productCategory models.ProductCategory

	setup.DB.Model(&models.ProductCategory{}).First(&productCategory, id)
	return productCategory
}

// Updates Everything, even falsy input values
func UpdateProductCategory(productCategory models.ProductCategory, userInput map[string]interface{}) models.ProductCategory {
	setup.DB.Model(&productCategory).Updates(userInput)
	return productCategory
}

// Deletes a product
func DeleteProductCategory(productCategory models.ProductCategory) models.ProductCategory {
	setup.DB.Delete(&productCategory)
	return productCategory
}
