package services

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
	"gorm.io/gorm"
)

// TODO: add this struct to validations package
type CreateProductInput struct {
	Name              string `form:"name" json:"name" binding:"required"`
	Price             int    `form:"price" json:"price" binding:"required"`
	DiscountPrice     int    `form:"discountPrice" json:"discountPrice"`
	Description       string `form:"description" json:"description" binding:"required"`
	Stock             int    `form:"stock" json:"stock"`
	ProductCategoryID uint   `form:"categoryID" json:"categoryID" binding:"required"`
}

type UpdateProductInput struct {
	Name              string `form:"name" json:"name"`
	Price             int    `form:"price" json:"price"`
	DiscountPrice     int    `form:"discountPrice" json:"discountPrice"`
	Description       string `form:"description" json:"description"`
	Stock             int    `form:"stock" json:"stock"`
	ProductCategoryID uint   `form:"categoryID" json:"categoryID" binding:"required"`
}

// Create new Prouct
func CreateProduct(input CreateProductInput, productCategory models.ProductCategory) (models.Product, error) {
	product := models.Product{
		Name:          input.Name,
		Price:         input.Price,
		DiscountPrice: input.DiscountPrice,
		Description:   input.Description,
		Stock:         input.Stock,
		// ProductCategoryID: input.ProductCategoryID,
		ProductCategory: productCategory,
	}

	err := setup.DB.Create(&product).Error
	return product, err
}

// Get all products
func FindProducts(paginate func(db *gorm.DB) *gorm.DB) ([]models.Product, int64, error) {
	var products []models.Product

	allProductsCount := setup.DB.Preload("ProductCategory").Model(&models.Product{}).Find(&products).RowsAffected
	queryBuilder := setup.DB.Preload("ProductCategory").Scopes(paginate).Order("id desc")
	result := queryBuilder.Model(&models.Product{}).Find(&products)
	
	return products, allProductsCount, result.Error
}

// Get a product by id
func FindProduct(id uint) (models.Product, error) {
	var product models.Product

	result := setup.DB.Preload("ProductCategory").Model(&models.Product{}).First(&product, id)
	return product, result.Error
}

// Update a product
// updates only non-zero input values
func UpdateProduct(product models.Product, userInput models.Product) (models.Product, error) {
	err := setup.DB.Model(&product).Updates(userInput).Error
	return product, err
}

// Deletes a product
func DeleteProduct(product models.Product) models.Product {
	setup.DB.Delete(&product)
	return product
}
