package controllers

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/zoccer-ecommerce/backend/api/features/products/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/boxing"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/pagination"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	imageStorage "gitlab.com/zoccer-ecommerce/backend/api/utils/storage"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

// Create new product
func CreateProduct(c *gin.Context) {
	// validate input TODO: move to validators middleware (package)
	var input services.CreateProductInput
	// This will infer what binder to use epending on the content-type header
	if err := c.ShouldBind(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// retrieve the product category, returning error if it doeesn't exist
	productCategory := services.FindProductCategory(input.ProductCategoryID)
	if productCategory.ID == 0 {
		response.SendError(c, http.StatusNotFound, "product category with id "+strconv.Itoa(int(input.ProductCategoryID))+" does not exist", nil)
		return
	}

	// check for image field
	file, fileHeader, err := c.Request.FormFile("image")
	if err != nil { // 'image' form field is missing (assumption) - option: check nil value of 'fileHeader'
		// TODO: reuse the 'Create Product' logic
		// Create Product
		product, err := services.CreateProduct(input, productCategory)
		if err != nil {
			response.SendError(c, http.StatusInternalServerError, "error creating product", err.Error())
			return
		}

		response.SendSuccess(c, http.StatusOK, "product created successfully", product)
		return
	}
	// 'image' form field is present
	defer file.Close()

	// Create Product
	product, err := services.CreateProduct(input, productCategory)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "error creating product", err.Error())
		return
	}

	// upload image to GCS
	folderName := "zoccer-dev/products"
	publicURL, err := imageStorage.UploadImage(c, file, fileHeader, folderName)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "cloud storage error", err.Error())
		return
	}

	// update the product with the 'publicURL'
	detailsToUpdate := models.Product{
		Image: boxing.BoxString(publicURL),
	}
	updatedProduct, err := services.UpdateProduct(product, detailsToUpdate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error updating product", err.Error())
		return
	}
	
	response.SendSuccess(c, http.StatusOK, "product created successfully", updatedProduct)
}

// Get all products
func FindProducts(c *gin.Context) {
	paginationValues := pagination.GeneratePaginationValues(c)
	paginate := pagination.Paginate(c)

	products, allProductsCount, err := services.FindProducts(paginate)
	if err != nil {
		fmt.Println(err)
		response.SendError(c, http.StatusBadRequest, "invalid input", err.Error())
		return
	}

	totalPages := math.Ceil(float64(allProductsCount) / float64(paginationValues.PageSize))
	data := struct {
		Count    int64            `json:"count"`
		Pages    int              `json:"pages"`
		Products []models.Product `json:"products"`
	}{
		Count:    allProductsCount,
		Pages:    int(totalPages),
		Products: products,
	}

	response.SendSuccess(c, http.StatusOK, "products retrieved successfully", data)
}

// Get a product by id
func FindProduct(c *gin.Context) {
	id := c.Param("productID")
	productID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as product id", nil)
		return
	}

	product, err := services.FindProduct(uint(productID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "product with id "+strconv.Itoa(productID)+" does not exist", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "product retrieved successfully", product)
}

// Updates a product
func UpdateProduct(c *gin.Context) {
	// find the product
	id := c.Param("productID")
	productID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as id", err.Error())
		return
	}
	product, err := services.FindProduct(uint(productID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "product with id "+strconv.Itoa(productID)+" does not exist", err.Error())
		return
	}

	// validate input
	var input services.UpdateProductInput
	if err := c.ShouldBind(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// retrieve the product category (return error if it doeesn't exist)
	var productCategory models.ProductCategory
	if input.ProductCategoryID != 0 { // user intends to change the product's category
		productCategory = services.FindProductCategory(input.ProductCategoryID)
		if productCategory.ID == 0 {
			response.SendError(c, http.StatusNotFound, "product category with id "+strconv.Itoa(int(input.ProductCategoryID))+" does not exist", nil)
			return
		}

		// update the product's category
		product.ProductCategory = productCategory
	}

	// check for image field
	file, fileHeader, err := c.Request.FormFile("image")
	if err != nil { // 'image' form field is missing (assumption) - option: check nil value of 'fileHeader'
		// TODO: reuse the 'Update Product' logic
		// Update Product
		userInput := models.Product{
			Name:              input.Name,
			Price:             input.Price,
			DiscountPrice:     input.DiscountPrice,
			Description:       input.Description,
			Stock:             input.Stock,
			ProductCategoryID: productCategory.ID,
		}
		updatedProduct, err := services.UpdateProduct(product, userInput)
		if err != nil {
			response.SendError(c, http.StatusInternalServerError, "error updating product", err.Error())
			return
		}

		response.SendSuccess(c, http.StatusOK, "product updated successfully", updatedProduct)
		return
	}
	// 'image' form field is present
	defer file.Close()

	// upload image to GCS
	folderName := "zoccer-dev/products"
	publicURL, err := imageStorage.UploadImage(c, file, fileHeader, folderName)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "cloud storage error", err.Error())
		return
	}

	userInput := models.Product{
		Name:              input.Name,
		Price:             input.Price,
		DiscountPrice:     input.DiscountPrice,
		Description:       input.Description,
		Stock:             input.Stock,
		ProductCategoryID: productCategory.ID,
		Image:             boxing.BoxString(publicURL),
	}
	updatedProduct, err := services.UpdateProduct(product, userInput)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error updating product", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "product updated successfully", updatedProduct)
}

// Deletes a product
func DeleteProduct(c *gin.Context) {
	// find the product
	id := c.Param("productID")
	productID, err := strconv.Atoi(id)
	if err != nil {
		response.SendSuccess(c, http.StatusBadRequest, "invalid id value", err.Error())
		return
	}

	product, err := services.FindProduct(uint(productID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "product with id "+strconv.Itoa(productID)+" does not exist", err.Error())
		return
	}
	// delete the product
	services.DeleteProduct(product)

	response.SendSuccess(c, http.StatusOK, "product deleted successfully", nil)
}
