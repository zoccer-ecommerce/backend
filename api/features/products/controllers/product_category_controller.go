package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/zoccer-ecommerce/backend/api/features/products/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"
)

// Create new product
func CreateProductCategory(c *gin.Context) {
	// validate input TODO: move to validations middleware (package)
	var input services.CreateProductCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// Create product category
	err, product := services.CreateProductCategory(input)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "error creating product", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "product category created successfully", product)
}

// Get all product categories
func FindProductCategories(c *gin.Context) {
	err, productCategories := services.FindProductCategories()
	if err != nil {
		fmt.Println(err)
		response.SendError(c, http.StatusBadRequest, "invalid input", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "product categories retrieved successfully", productCategories)
}

// Get a product category by id
func FindProductCategory(c *gin.Context) {
	id := c.Param("categoryID")
	categoryID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as category id", nil)
		return
	}

	productCategory := services.FindProductCategory(uint(categoryID))
	if productCategory.ID == 0 {
		response.SendError(c, http.StatusNotFound, "product category with id "+strconv.Itoa(categoryID)+" does not exist", nil)
		return
	}

	response.SendSuccess(c, http.StatusOK, "product category retrieved successfully", productCategory)
}

// Updates a product category
func UpdateProductCategory(c *gin.Context) {
	// find the productCategory
	id := c.Param("categoryID")
	categoryID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as category id", err.Error())
		return
	}
	productCategory := services.FindProductCategory(uint(categoryID))
	if productCategory.ID == 0 {
		response.SendError(c, http.StatusNotFound, "product category with id "+strconv.Itoa(categoryID)+" does not", nil)
		return
	}

	// validate input
	var input services.UpdateProductCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	userInput := map[string]interface{}{
		"name": input.Name,
	}

	// update the productCategory
	updatedProductCategory := services.UpdateProductCategory(productCategory, userInput)

	response.SendSuccess(c, http.StatusOK, "product category updated successfully", updatedProductCategory)
}

// Deletes a product category
func DeleteProductCategory(c *gin.Context) {
	// find the product category
	id := c.Param("categoryID")
	categoryID, err := strconv.Atoi(id)
	if err != nil {
		response.SendSuccess(c, http.StatusBadRequest, "invalid id value", err.Error())
		return
	}
	productCategory := services.FindProductCategory(uint(categoryID))
	if productCategory.ID == 0 {
		response.SendError(c, http.StatusNotFound, "product category with id "+strconv.Itoa(categoryID)+" does not", nil)
		return
	}
	// delete the productCategory
	services.DeleteProductCategory(productCategory)

	response.SendSuccess(c, http.StatusOK, "product category ("+productCategory.Name+") deleted successfully", nil)
}
