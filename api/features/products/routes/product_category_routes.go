package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/products/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)

func ProductCategories(routesGroup *gin.RouterGroup) *gin.RouterGroup {
	productCategoriesApi := routesGroup.Group("/product-categories")
	{
		productCategoriesApi.POST("/", middleware.AuthGuard(), middleware.AdminGuard(), controllers.CreateProductCategory)
		productCategoriesApi.GET("/", controllers.FindProductCategories)
		productCategoriesApi.GET("/:categoryID", controllers.FindProductCategory)
		productCategoriesApi.PATCH("/:categoryID",middleware.AuthGuard(), middleware.AdminGuard(), controllers.UpdateProductCategory)
		productCategoriesApi.DELETE("/:categoryID", middleware.AuthGuard(), middleware.AdminGuard(), controllers.DeleteProductCategory)
	}

	return productCategoriesApi
}
