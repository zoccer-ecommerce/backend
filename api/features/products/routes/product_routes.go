package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/products/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)

func Products(baseApiV1 *gin.RouterGroup) *gin.RouterGroup {
	productsApi := baseApiV1.Group("/products")
	{
		// product routes
		productsApi.GET("/", controllers.FindProducts)
		productsApi.GET("/:productID/", controllers.FindProduct)
		productsApi.POST("/", middleware.AuthGuard(), middleware.AdminGuard(), controllers.CreateProduct)
		productsApi.PATCH("/:productID/", middleware.AuthGuard(), middleware.AdminGuard(), controllers.UpdateProduct)
		productsApi.DELETE("/:productID/", middleware.AuthGuard(), middleware.AdminGuard(), controllers.DeleteProduct)
	}

	return productsApi
}
