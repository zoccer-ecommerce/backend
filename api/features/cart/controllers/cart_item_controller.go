package controllers

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/zoccer-ecommerce/backend/api/features/cart/services"
	productService "gitlab.com/zoccer-ecommerce/backend/api/features/products/services"
	userService "gitlab.com/zoccer-ecommerce/backend/api/features/users/services"
	"gitlab.com/zoccer-ecommerce/backend/db/models"

	"gitlab.com/zoccer-ecommerce/backend/api/utils/auth"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/pagination"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"
)

// Add cart item or increment cart item quantity
func AddCartItem(c *gin.Context) {
	// validate input
	var input services.AddCartItemInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// Retrieving user claims from auth token
	// TODO: avoid code duplication when checking for 'userTokenData' and during type assertions
	userTokenData, exists := c.Get("userTokenData")
	if exists == false { // user token data does not exist
		response.SendError(c, http.StatusInternalServerError, "an error occurred", "userTokenData is missing")
		return
	}
	claims, ok := userTokenData.(*auth.JWTClaims) // type assertion
	if !ok {                                      // type assertion not successfully
		response.SendError(c, http.StatusInternalServerError, "an error occurred", "type asserion failed")
		return
	}

	// making sure the product exists
	foundProduct, err := productService.FindProduct(uint(input.ProductID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "product with id "+strconv.Itoa(input.ProductID)+" does not exist", err.Error())
		return
	}

	// obataining the user's cart
	foundCart, err := services.FindUserCart(uint(claims.UserID))
	if err != nil {
		if err.Error() == "record not found" { // cart is absent
			// Create Cart
			createdCart, err := services.CreateCart("pending", uint(claims.UserID)) // TODO: how to encapsulate "pending" to reuse
			if err != nil {
				response.SendError(c, http.StatusBadRequest, "error creating cart", err.Error())
				return
			}

			// Add the Cart Item
			addedCartItem, err := services.AddCartItem(createdCart.ID, foundProduct)
			if err != nil {
				response.SendError(c, http.StatusInternalServerError, "error adding cart item", err.Error())
				return
			}

			response.SendSuccess(c, http.StatusOK, "cart item added successfully", addedCartItem)
			return
		}

		// other errors when retrieving a user's cart
		response.SendError(c, http.StatusInternalServerError, "error retrieving user cart", err.Error())
		return
	}

	// cart is present
	// check if the cart item (product) is already present in the cart
	foundCartItem, err := services.FindCartItemByProductIDAndCartID(uint(input.ProductID), foundCart.ID)
	if err != nil {
		if err.Error() == "record not found" { // cart item not found
			// Add the Cart Item
			addedCartItem, err := services.AddCartItem(foundCart.ID, foundProduct)
			if err != nil {
				response.SendError(c, http.StatusInternalServerError, "error adding cart item", err.Error())
				return
			}

			response.SendSuccess(c, http.StatusOK, "cart item added successfully", addedCartItem)
			return
		}

		// other errors when retrieving a user's cart
		response.SendError(c, http.StatusInternalServerError, "error retrieving cart item", err.Error())
		return
	} else { // cart item found
		// increment cart item quantity
		detailsToUpdate := models.CartItem{
			Quantity: foundCartItem.Quantity + 1,
		}
		updatedCartItem, err := services.UpdateCartItem(foundCartItem, detailsToUpdate)
		if err != nil {
			response.SendError(c, http.StatusInternalServerError, "error updating cart item quantity", err.Error())
			return
		}

		response.SendSuccess(c, http.StatusOK, "cart item quantity updated successfully", updatedCartItem)
		return
	}

}

// Retrieve the logged-in user cart items
func FindCartItems(c *gin.Context) {
	paginationValues := pagination.GeneratePaginationValues(c)
	paginate := pagination.Paginate(c)

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// TODO: enable admins to be able to retrieve a user's cart

	// check whether the user exists
	err, foundUser := userService.FindUser(uint(claims.UserID))
	if err != nil {
		// TODO: also check for other possible errors apart from 'record not found'
		msg := fmt.Sprintf("user with id %v not found", claims.UserID)
		response.SendError(c, http.StatusNotFound, msg, err.Error())
		return
	}

	// obtain the user's cart
	foundUserCart, err := services.FindUserCart(foundUser.ID)
	if err != nil {
		// TODO: also check for other possible errors apart from 'record not found' error
		data := struct {
			Count     int64             `json:"count"`
			Pages     int               `json:"pages"`
			CartItems []models.CartItem `json:"cartItems"`
		}{
			Count:     0,
			Pages:     0,
			CartItems: []models.CartItem{},
		}
		response.SendError(c, http.StatusNotFound, "user cart is empty", data)
		return
	}

	// retrieve the cart items
	foundCartItems, cartItemsCount, err := services.FindCartItems(foundUserCart.ID, paginate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error retrieving cart items", err.Error())
		return
	}

	totalPages := math.Ceil(float64(cartItemsCount) / float64(paginationValues.PageSize))
	data := struct {
		Count     int64             `json:"count"`
		Pages     int               `json:"pages"`
		CartItems []models.CartItem `json:"cartItems"`
	}{
		Count:     cartItemsCount,
		Pages:     int(totalPages),
		CartItems: foundCartItems,
	}

	response.SendSuccess(c, http.StatusOK, "cart items retrieved successfully", data)
}

// Update cart item's quantity
func UpdateCartItem(c *gin.Context) {
	// validate input
	var input services.UpdateCartItemInput
	// TODO: ensure 'quantity' is not set to zero (0) by the client
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// Check first if the product exists
	foundProduct, err := productService.FindProduct(uint(input.ProductID))
	if err != nil {
		msg := fmt.Sprintf("product with id %v not found", input.ProductID)
		response.SendError(c, http.StatusNotFound, msg, err.Error())
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// find the user's cart
	foundUserCart, err := services.FindUserCart(uint(claims.UserID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "user has no cart", err.Error())
		return
	}

	// check if the cart item is already present in the cart
	foundCartItem, err := services.FindCartItemByProductIDAndCartID(foundProduct.ID, foundUserCart.ID)
	if err != nil {
		response.SendError(c, http.StatusNotFound, "the cart item does not exist", err.Error())
		return
	}

	detailsToUpdate := models.CartItem{
		Quantity: input.Quantity,
	}

	// TODO: check 'input.Quantity'. If it's zero (0), call service to remove the cart item

	updatedCartItem, err := services.UpdateCartItem(foundCartItem, detailsToUpdate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error updating cart item quantity", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "cart item quantity updated successfully", updatedCartItem)
	return
}

// Remove all cart item quantity from a user's cart
func DeleteCartItem(c *gin.Context) {
	// validate input
	var input services.DeleteCartItemInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// Check first if the product exists
	foundProduct, err := productService.FindProduct(uint(input.ProductID))
	if err != nil {
		msg := fmt.Sprintf("product with id %v not found", input.ProductID)
		response.SendError(c, http.StatusNotFound, msg, err.Error())
		return
	}

	// Retrieving user claims from auth token
	claims, err := auth.RetrieveUserClaims(c)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	// find the user's cart
	foundUserCart, err := services.FindUserCart(uint(claims.UserID))
	if err != nil {
		response.SendError(c, http.StatusNotFound, "user has no cart", err.Error())
		return
	}

	// check if the cart item is already present in the cart
	foundCartItem, err := services.FindCartItemByProductIDAndCartID(foundProduct.ID, foundUserCart.ID)
	if err != nil {
		response.SendError(c, http.StatusNotFound, "the cart item does not exist", err.Error())
		return
	}

	// remove the cart item
	if _, err := services.DeleteCartItem(foundCartItem); err != nil {
		response.SendError(c, http.StatusInternalServerError, "error removing cart item", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "cart item removed successfully", nil)
}
