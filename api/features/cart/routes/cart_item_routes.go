package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/cart/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)

func CartItems(routesGroup *gin.RouterGroup) *gin.RouterGroup {
	cartItemsApi := routesGroup.Group("/cart-items")
	{
		cartItemsApi.POST("/", middleware.AuthGuard(), controllers.AddCartItem)
		cartItemsApi.GET("/", middleware.AuthGuard(), controllers.FindCartItems)
		cartItemsApi.PATCH("/", middleware.AuthGuard(), controllers.UpdateCartItem)
		cartItemsApi.DELETE("/", middleware.AuthGuard(), controllers.DeleteCartItem)
	}

	return cartItemsApi
}