package services

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
	"gorm.io/gorm"
)

type AddCartItemInput struct {
	ProductID int `json:"productID" binding:"required"`
}
type UpdateCartItemInput struct {
	ProductID int `json:"productID" binding:"required"`
	Quantity  int `json:"quantity" binding:"required"`
}
type DeleteCartItemInput struct {
	ProductID int `json:"productID" binding:"required"`
}

// Create new Cart
func CreateCart(status string, userID uint) (models.Cart, error) {
	cart := models.Cart{
		Status: status,
		UserID: userID,
		// User:   user,
	}

	err := setup.DB.Create(&cart).Error
	return cart, err
}

// Retrieve User Cart (retrieves only "pending" user's cart)
func FindUserCart(userID uint) (models.Cart, error) {
	var cart models.Cart

	result := setup.DB.Preload("User").Where("user_id = ? AND status = ?", userID, "pending").Model(&models.Cart{}).First(&cart)
	return cart, result.Error
}

// Adding Cart Item to Cart
func AddCartItem(cartID uint, product models.Product) (models.CartItem, error) {
	cartItem := models.CartItem{
		Quantity: 1,
		CartID:   cartID,
		Product:  product,
		// ProductID: productID,
	}

	err := setup.DB.Create(&cartItem).Error
	return cartItem, err
}

// Retrieving Cart Items (paginated)
func FindCartItems(cartID uint, paginate func(db *gorm.DB) *gorm.DB) ([]models.CartItem, int64, error) {
	var cartItems []models.CartItem

	allItemsCount := setup.DB.Preload("Product").Preload("Product.ProductCategory").Where("cart_id = ?", cartID).Model(&models.CartItem{}).Find(&cartItems).RowsAffected
	queryBuilder := setup.DB.Preload("Product").Preload("Product.ProductCategory").Where("cart_id = ?", cartID).Scopes(paginate).Order("id desc")
	result := queryBuilder.Where("cart_id = ?", cartID).Model(&models.CartItem{}).Find(&cartItems)

	return cartItems, allItemsCount, result.Error
}

// Retrieving Cart Items (not paginated)
func FindCartItemsWithoutPagination(cartID uint) ([]models.CartItem, int64, error) {
	// TODO: refactor and share reusable parts with 'FindCartItems' function
	var cartItems []models.CartItem

	allItemsCount := setup.DB.Preload("Product").Preload("Product.ProductCategory").Where("cart_id = ?", cartID).Model(&models.CartItem{}).Find(&cartItems).RowsAffected
	queryBuilder := setup.DB.Preload("Product").Preload("Product.ProductCategory").Where("cart_id = ?", cartID).Order("id desc")
	result := queryBuilder.Where("cart_id = ?", cartID).Model(&models.CartItem{}).Find(&cartItems)

	return cartItems, allItemsCount, result.Error
}

// Retreve Cart Item based on product id and cartid
func FindCartItemByProductIDAndCartID(productID, cartID uint) (models.CartItem, error) {
	var cartItem models.CartItem

	result := setup.DB.Preload("Product").Preload("Product.ProductCategory").Where("product_id = ? AND cart_id = ?", productID, cartID).Model(&models.CartItem{}).First(&cartItem)
	return cartItem, result.Error
}

// Update Cart
func UpdateCart(cart models.Cart, detailsToUpdate models.Cart) (models.Cart, error) {
	err := setup.DB.Model(&cart).Updates(detailsToUpdate).Error
	return cart, err
}

// Update Cart Item Quantity
func UpdateCartItem(cartItem models.CartItem, detailsToUpdate models.CartItem) (models.CartItem, error) {
	err := setup.DB.Model(&cartItem).Updates(detailsToUpdate).Error
	return cartItem, err
}

// Clear a user's cart item from cart
func DeleteCartItem(cartItem models.CartItem) (models.CartItem, error) {
	result := setup.DB.Delete(&cartItem)
	return cartItem, result.Error
}
