package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/payments/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)


func Payments(routesGroup *gin.RouterGroup) *gin.RouterGroup {
	paypalApi := routesGroup.Group("/payments/paypal")
	{
		paypalApi.POST("/order-create-request", middleware.AuthGuard(), controllers.CreateOrderRequest)
		paypalApi.POST("/order-capture-request", middleware.AuthGuard(), controllers.CaptureOrderRequest)
	}

	return paypalApi
}
