package controllers

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/zoccer-ecommerce/backend/api/features/payments/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"
)

// TODO: abstract common functionality when calling services below in its own package, and reuse where needed

func CreateOrderRequest(c *gin.Context) {
	// validate input TODO: move to validations middleware (package)
	var input services.CreateOrderRequestInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// call the service
	respBody, err := services.CreateOrderRequest(input.Amount)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error creating paypal order request", err.Error())
		return
	}

	// check for service call errors
	if respBody.Status == "error" {
		fmt.Printf("\nError -> %v\n\n", respBody)

		response.SendError(c, http.StatusInternalServerError, "error creating paypal order request", respBody.Message)
		return
	}

	response.SendSuccess(c, http.StatusOK, "successfully created paypal order request", respBody.Data)
}

func CaptureOrderRequest(c *gin.Context) {
	// validate input TODO: move to validations middleware (package)
	var input services.CaptureOrderRequestInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// call the service
	respBody, err := services.CaptureOrderRequest(input.OrderID)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "error capturing paypal order request", err.Error())
		return
	}

	fmt.Printf("\nResponse Body -> %v\n\n", respBody)

	// check for service call errors
	if respBody.Status == "error" {
		fmt.Printf("\nError -> %v\n\n", respBody)

		// body := struct {
		// 	Message string      `json:"message"`
		// 	Data    interface{} `json:"data"`
		// }{
		// 	Message: respBody.Message,
		// 	Data:    respBody.Data,
		// }

		response.SendError(c, http.StatusInternalServerError, "error capturing paypal order request", respBody.Data)
		return
	}

	response.SendSuccess(c, http.StatusOK, "successfully captured paypal order request", respBody.Data)
}
