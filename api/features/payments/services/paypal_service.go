package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type OrderCreateRequestResponseBody struct {
	Status  string                         `json:"status"`
	Message string                         `json:"message"`
	Data    OrderCreateRequestResponseData `json:"data"`
}

type CreateOrderRequestInput struct {
	Amount int `json:"amount" binding:"required"`
}

type OrderCreateRequestBody struct {
	Amount int `json:"amount"`
}

type OrderCreateRequestResponseData struct {
	OrderID string `json:"orderID"`
}

type OrderCaptureRequestResponseBody struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"` // TODO: make use of 'OrderCaptureRequestResponseData'
}

type CaptureOrderRequestInput struct {
	OrderID string `json:"orderID" binding:"required"`
}

type OrderCaptureRequestBody struct {
	OrderID string `json:"orderId"`
}

// type OrderCaptureRequestResponseData struct {
// 	OrderID string `json:"orderID"`
// }

// TODO: abstract calling external REST APIs to its oe=wn package, and reuse where needed

// var paypalBaseAPI = os.Getenv("PAYPAL_BASE_API")

func CreateOrderRequest(amount int) (*OrderCreateRequestResponseBody, error) {
	jsonReq, err := json.Marshal(OrderCreateRequestBody{
		Amount: amount,
	})
	if err != nil {
		return nil, err
	}

	paypalBaseAPI := os.Getenv("PAYPAL_BASE_API")
	url := fmt.Sprintf("%v/%v", paypalBaseAPI, "payments/orders-create-request")

	resp, err := http.Post(
		url,
		"application/json",
		bytes.NewBuffer(jsonReq),
	)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	// convert response body to struct
	var responseBody OrderCreateRequestResponseBody
	json.Unmarshal(bodyBytes, &responseBody)

	return &responseBody, nil
}

func CaptureOrderRequest(orderID string) (*OrderCaptureRequestResponseBody, error) {
	jsonReq, err := json.Marshal(OrderCaptureRequestBody{
		OrderID: orderID,
	})
	if err != nil {
		return nil, err
	}

	paypalBaseAPI := os.Getenv("PAYPAL_BASE_API")
	url := fmt.Sprintf("%v/%v", paypalBaseAPI, "payments/orders-capture-request")

	resp, err := http.Post(
		url,
		"application/json",
		bytes.NewBuffer(jsonReq),
	)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	// convert response body to struct
	var responseBody OrderCaptureRequestResponseBody
	json.Unmarshal(bodyBytes, &responseBody)

	return &responseBody, nil
}
