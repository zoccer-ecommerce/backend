package controllers

import (
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/users/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/boxing"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/pagination"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	imageStorage "gitlab.com/zoccer-ecommerce/backend/api/utils/storage"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

// Retrieve all users
func FindUsers(c *gin.Context) {
	paginationValues := pagination.GeneratePaginationValues(c)
	paginate := pagination.Paginate(c)

	users, allUsersCount, err := services.FindUsers(paginate)
	if err != nil {
		fmt.Println(err)
		response.SendError(c, http.StatusBadRequest, "invalid input", err.Error())
		return
	}

	totalPages := math.Ceil(float64(allUsersCount) / float64(paginationValues.PageSize))
	data := struct {
		Count int64         `json:"count"`
		Pages int           `json:"pages"`
		Users []models.User `json:"users"`
	}{
		Count: allUsersCount,
		Pages: int(totalPages),
		Users: users,
	}

	response.SendSuccess(c, http.StatusOK, "users retrieved successfully", data)
}

// Retrieve a user by id
func FindUser(c *gin.Context) {
	id := c.Param("userID")
	userID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as user id", nil)
		return
	}

	err, user := services.FindUser(uint(userID))
	if user.ID == 0 {
		response.SendError(c, http.StatusNotFound, "user with id "+strconv.Itoa(userID)+" does not exist", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "user retrieved successfully", user)
}

// Update a user
func UpdateUser(c *gin.Context) {
	// find the user
	id := c.Param("userID")
	userID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "please enter a valid integer as user id", err.Error())
		return
	}
	err, user := services.FindUser(uint(userID))
	if user.ID == 0 {
		response.SendError(c, http.StatusNotFound, "user with id "+strconv.Itoa(userID)+" does not exist", err.Error())
		return
	}

	// validate input
	var input services.UpdateUserInput
	if err := c.ShouldBind(&input); err != nil {
		response.SendError(c, http.StatusBadRequest, "incorrect input", err.Error())
		return
	}

	// TODO: add email validation (if present in input)

	// TODO: add phoneNUmber validation (if present in input)

	// check for image field TODO: refactor and reuse image upload logic when updating a product
	file, fileHeader, err := c.Request.FormFile("image")
	if err != nil { // 'image' form field is missing (assumption) - option: check nil value of 'fileHeader'
		// TODO: reuse the 'Update User' logic
		// Update User
		userInput := models.User{
			Username: input.Username,
			// Firstname:   input.Firstname,
			// Lastname:    input.Lastname,
			Firstname:   boxing.BoxString(input.Firstname),
			Lastname:    boxing.BoxString(input.Lastname),
			Email:       input.Email,
			PhoneNumber: input.PhoneNumber,
		}
		// TODO: handle error omitted below
		err, updatedUser := services.UpdateUser(user, userInput)
		if err != nil {
			response.SendError(c, http.StatusInternalServerError, "failed to update user", err.Error())
			return
		}
		response.SendSuccess(c, http.StatusOK, "user updated successfully", updatedUser)
		return
	}
	// 'image' form field is present
	defer file.Close()

	// upload image to GCS
	folderName := "zoccer-dev/users"
	publicURL, err := imageStorage.UploadImage(c, file, fileHeader, folderName)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "cloud storage error", err.Error())
		return
	}

	fmt.Printf("\nF Name -> %v...L Name -> %v\n\n", input.Firstname, input.Lastname)

	userInput := models.User{
		Username:    input.Username,
		Firstname:   boxing.BoxString(input.Firstname),
		Lastname:    boxing.BoxString(input.Lastname),
		Email:       input.Email,
		PhoneNumber: input.PhoneNumber,
		Image:       boxing.BoxString(publicURL),
	}

	// update the user
	err, updatedUser := services.UpdateUser(user, userInput)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "failed to update user", err.Error())
		return
	}

	// TODO: investigate why updating a user doesn't return null fields (eg, in frstname, lastname and image)
	response.SendSuccess(c, http.StatusOK, "user updated successfully", updatedUser)
}

// Delete a user
func DeleteUser(c *gin.Context) {
	id := c.Param("userID")
	userID, err := strconv.Atoi(id)
	if err != nil {
		response.SendError(c, http.StatusBadRequest, "invalid user id value", err.Error())
		return
	}

	// find the user
	_, user := services.FindUser(uint(userID))
	if user.ID == 0 {
		response.SendError(c, http.StatusNotFound, "user with id "+strconv.Itoa(userID)+" does not", nil)
		return
	}

	// delete the user
	if err, _ := services.DeleteUser(user.ID); err != nil {
		response.SendError(c, http.StatusInternalServerError, "faled to delete user", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "user deleted successfully", nil)
}
