package services

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
	"gorm.io/gorm"
)

type UpdateUserInput struct {
	Username    string `form:"username" json:"username"`
	Firstname   string `form:"firstname" json:"firstname"`
	Lastname    string `form:"lastname" json:"lastname"`
	Email       string `form:"email" json:"email"`
	PhoneNumber string `form:"phoneNumber" json:"phoneNumber"`
}

// Retrieve all users
func FindUsers(paginate func(db *gorm.DB) *gorm.DB) ([]models.User, int64, error) {
	var users []models.User

	allUsersCount := setup.DB.Model(&models.User{}).Find(&users).RowsAffected
	queryBuilder := setup.DB.Scopes(paginate).Order("id desc")
	result := queryBuilder.Model(&models.User{}).Find(&users)
	
	return users, allUsersCount, result.Error
}

// Retrieve a user by id
func FindUser(userId uint) (error, models.User) {
	var user models.User
	err := setup.DB.Model(&models.User{}).First(&user, userId).Error
	return err, user
}
func FindAPIUser(userId uint) (error, models.APIUser) {
	var apiUser models.APIUser
	err := setup.DB.Model(&models.User{}).First(&apiUser, userId).Error
	return err, apiUser
}

// Update a user
// updates only non-zero input values
func UpdateUser(user models.User, userInput models.User) (error, models.User) {
	err := setup.DB.Model(&user).Updates(userInput).Error
	return err, user
}

// updates user password reset and pasword reset expires to null
func ResetPasswordResetValues(user models.User, userInput models.User) (error, models.User) {
	err := setup.DB.Model(&user).Select("PasswordResetToken", "PasswordResetExpires").Updates(userInput).Error
	return err, user
}

// Delete a user
func DeleteUser(userID uint) (error, models.User) {
	var result models.User
	err := setup.DB.Where("id = ?", userID).Delete(&result).Error
	return err, result
}
