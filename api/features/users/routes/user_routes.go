package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/api/features/users/controllers"
	"gitlab.com/zoccer-ecommerce/backend/api/middleware"
)

func Users(baseApiV1 *gin.RouterGroup) *gin.RouterGroup {
	usersApi := baseApiV1.Group("/users")
	{
		usersApi.GET("/", middleware.AuthGuard(), middleware.AdminGuard(), controllers.FindUsers)
		usersApi.GET("/:userID", middleware.AuthGuard(), middleware.OwnUserGuard(), controllers.FindUser)
		usersApi.PATCH("/:userID", middleware.AuthGuard(), middleware.OwnUserGuard(), controllers.UpdateUser)
		usersApi.DELETE("/:userID", middleware.AuthGuard(), middleware.AdminGuard(), controllers.DeleteUser)
	}

	return usersApi
}
