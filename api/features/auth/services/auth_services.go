package services

import (
	"time"

	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

type CreateUserInput struct {
	Username    string `json:"username" binding:"required"`
	Email       string `json:"email" binding:"required"` // TODO: email validation
	Password    string `json:"password" binding:"required"`
	PhoneNumber string `json:"phoneNumber" binding:"required"` // TODO: validating phone number
}

type LoginUserInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type LogoutUserInput struct {
	RefreshToken string `json:"refreshToken" binding:"required"`
}

type GenerateNewAccessTokenInput struct {
	RefreshToken string `json:"refreshToken" binding:"required"`
}

type ForgotPasswordInput struct {
	Email string `json:"email" binding:"required"`
}

type NewPasswordInput struct {
	Password string `json:"password" binding:"required"`
	Token    string `json:"token" binding:"required"`
}

// Create new user
func CreateUser(input CreateUserInput) (models.User, error) {
	newUser := models.User{
		Username:    input.Username,
		Email:       input.Email,
		Password:    input.Password,
		PhoneNumber: input.PhoneNumber,
	}

	err := setup.DB.Create(&newUser).Error
	// TODO: find out how to return selected field (ie. exclude password from response)
	return newUser, err
}

// Retrieve a user by username
func FindUserByUsername(userName string) (error, models.User) {
	var user models.User
	err := setup.DB.Where("username = ?", userName).First(&user).Error
	return err, user
}

// TODO: add refresh token functionality to its own service file
// Save refresh token
func SaveRefreshToken(refreshToken string) (error, string) {
	tokenResult := models.RefreshToken{
		Token: refreshToken,
	}
	err := setup.DB.Create(&tokenResult).Error

	return err, tokenResult.Token
}

// Retrieve refresh token
func FindRefreshToken(refreshToken string) (error, models.RefreshToken) {
	var tokenResult models.RefreshToken
	err := setup.DB.Where("token = ?", refreshToken).First(&tokenResult).Error
	return err, tokenResult
}

// Delete refresh token
func DeleteRefreshToken(refreshToken string) (error, models.RefreshToken) {
	var tokenResult models.RefreshToken
	err := setup.DB.Where("token = ?", refreshToken).Delete(&tokenResult).Error
	return err, tokenResult
}

// Retrieve a user by email address
func FindUserByEmail(email string) (error, models.User) {
	var user models.User
	err := setup.DB.Where("email = ?", email).First(&user).Error
	return err, user
}

// Retrieve a user by password reset token
func FindUserByPasswordResetToken(token string) (error, models.User) {
	var user models.User
	
	// timestamp in milliseconds
	timeNow := time.Now().UnixNano() / 1e+6
	timeNowUnix := time.Unix(timeNow / 1e+3, 0)

	err := setup.DB.Where("password_reset_token = ?", token).Where("password_reset_expires > ?", timeNowUnix).First(&user).Error
	return err, user
}
