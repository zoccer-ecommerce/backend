package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/badoux/checkmail"
	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"github.com/sendgrid/sendgrid-go/helpers/mail"
	authService "gitlab.com/zoccer-ecommerce/backend/api/features/auth/services"
	userService "gitlab.com/zoccer-ecommerce/backend/api/features/users/services"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/auth"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/boxing"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/validations"

	"gitlab.com/zoccer-ecommerce/backend/config/email"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

// Create new user
func CreateUser(c *gin.Context) {
	// validate input
	var input authService.CreateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}
	// check email validity
	if emailErr := checkmail.ValidateFormat(input.Email); emailErr != nil {
		response.SendError(c, http.StatusBadRequest, "incorrect email format", emailErr.Error())
		return
	}
	// TODO: check phoneNumber validity

	// hash the password
	hashedPassword, hashErr := auth.HashPassword(input.Password)
	if hashErr != nil {
		response.SendError(c, http.StatusInternalServerError, "password could not be hashed", hashErr.Error())
		return
	}
	input.Password = string(hashedPassword)

	// TODO: add other input fields (eg. Image) as nil

	// Create User
	createdUser, err := authService.CreateUser(input)
	if err != nil {
		fmt.Println(err)
		response.SendError(c, http.StatusBadRequest, "invalid input", err.Error())
		return
	}
	response.SendSuccess(c, http.StatusOK, "user created successfully", createdUser)
}

// Login a user
func LoginUser(c *gin.Context) {
	// validate input
	var input authService.LoginUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	err, foundUser := authService.FindUserByUsername(input.Username)
	if err != nil {
		msg := fmt.Sprintf("incorrect username or password")
		response.SendError(c, http.StatusBadRequest, msg, err.Error())
		return
	}

	if err := auth.VerifyPassword(foundUser.Password, input.Password); err != nil {
		response.SendError(c, http.StatusBadRequest, "incorrect username or password", err.Error())
		return
	}

	accessToken, err := auth.GenerateAccessToken(foundUser)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not generate access token", err.Error())
		return
	}
	refreshToken, err := auth.GenerateRefreshToken(foundUser)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not generate refresh token", err.Error())
		return
	}

	// make a record of the refresh token in th db
	if err, _ := authService.SaveRefreshToken(refreshToken); err != nil {
		response.SendError(c, http.StatusInternalServerError, "error saving refresh token", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "user logged in successfully", auth.Token{Access: accessToken, Refresh: refreshToken})
}

// Logout a user
func LogoutUser(c *gin.Context) {
	// validate input
	var input authService.LogoutUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// ensure the refresh token exists
	if err, _ := authService.FindRefreshToken(input.RefreshToken); err != nil {
		response.SendError(c, http.StatusNotFound, "the refresh token provided does not exist", err.Error())
		return
	}

	if err, _ := authService.DeleteRefreshToken(input.RefreshToken); err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "user logged out successfully", nil)
}

// Generate new Access and Refresh tokens
func GenerateNewAuthTokens(c *gin.Context)  {
	// validate input
	var input authService.GenerateNewAccessTokenInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// ensure the refresh token exists
	if err, _ := authService.FindRefreshToken(input.RefreshToken); err != nil {
		response.SendError(c, http.StatusNotFound, "the refresh token provided does not exist", err.Error())
		return
	}
	
	// verify the refresh token
	claims, err := auth.VerifyRefreshToken(input.RefreshToken)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not verify refresh token", err.Error())
		return
	}

	// generate new tokens
	err, foundUser := userService.FindUser(uint(claims.UserID))
	if err != nil {
		msg := fmt.Sprintf("user with id %d not found", claims.UserID)
		response.SendError(c, http.StatusNotFound, msg, err.Error())
		return
	}
	accessToken, err := auth.GenerateAccessToken(foundUser)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not generate access token", err.Error())
		return
	}
	refreshToken, err := auth.GenerateRefreshToken(foundUser)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not generate refresh token", err.Error())
		return
	}
	// delete the previous refresh token
	if err, _ := authService.DeleteRefreshToken(input.RefreshToken); err != nil {
		response.SendError(c, http.StatusInternalServerError, "an error occurred", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "new auth tokens generated successfully", auth.Token{Access: accessToken, Refresh: refreshToken})
}

// sending password reset email to the user's email address
func SendPasswordResetEmail(c *gin.Context) {
	// validate input
	var input authService.ForgotPasswordInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// TODO: move similar validation login to a separate package (and reuse where neccessary)
	// check email validity
	if err := checkmail.ValidateFormat(input.Email); err != nil {
		response.SendError(c, http.StatusBadRequest, "invalid email format", err.Error())
		return
	}

	// retrieve user by the provided email address
	err, foundUser := authService.FindUserByEmail(input.Email)
	if err != nil {
		msg := fmt.Sprintf("user with email %s does not exist", input.Email)
		response.SendError(c, http.StatusNotFound, msg, err.Error())
		return
	}

	// generate password reset token
	passwordResetToken, err := auth.GeneratePasswordResetToken(3)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "could not generate password reset token", err.Error())
		return
	}
	// get the time the password is supposed to expire
	passwordResetExpires := auth.PasswordResetExpires()

	// update the user's password reset token and its expiry time
	userDetailsToUpdate := models.User{
		PasswordResetToken:   boxing.BoxString(passwordResetToken),
		PasswordResetExpires: boxing.BoxTime(passwordResetExpires),
	}
	err, updatedUser := userService.UpdateUser(foundUser, userDetailsToUpdate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "failed to update user", err.Error())
		return
	}

	// send email to the user
	url := location.Get(c)
	protocol := url.Scheme           // client protocol
	host := os.Getenv("CLIENT_HOST") // client host

	// fmt.Printf("\nHost => %v...Protocol => %v\n\n", host, protocol)

	clientPasswordResetURL := fmt.Sprintf("\n%s://%s/auth/new-password/%s\n\n", protocol, host, passwordResetToken)

	// fmt.Printf("\nclient url => %s\n", clientPasswordResetURL)

	recipientEmail := updatedUser.Email
	recipientUsername := updatedUser.Username
	passwordResetHTMLLinkTag := fmt.Sprintf(`<a href="%s">Password Reset Link</a>`, clientPasswordResetURL)

	// fmt.Printf("\nhref => %s\n\n", passwordResetHTMLLinkTag)

	from := mail.NewEmail("Zoccer Admin", os.Getenv("FROM_EMAIL"))
	subject := "Password Reset"
	to := mail.NewEmail(recipientUsername, recipientEmail)
	plainTextContent := ""
	htmlContent := fmt.Sprintf(`
			<span>Hello <strong>%s</strong></span>
			<br />
			<p>You requested password reset for your Zoccer account.</p>
			<p>Please click on the link below to complete your password reset process.</p>
			%s
			<br />
			`, recipientUsername, passwordResetHTMLLinkTag)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	client := email.EmailClient()
	if _, err := client.Send(message); err != nil {
		response.SendError(c, http.StatusInternalServerError, "Error sending password reset email", err.Error())
		return
	}

	msg := fmt.Sprintf("Your password reset instructions have been successfully sent to %s", updatedUser.Email)
	data := struct {
		PasswordResetToken string `json:"token"`
	}{
		PasswordResetToken: passwordResetToken,
	}
	response.SendSuccess(c, http.StatusOK, msg, data)
}

// setting new password
func SetNewUserPassword(c *gin.Context) {
	// validate input
	var input authService.NewPasswordInput
	if err := c.ShouldBindJSON(&input); err != nil {
		var validationErr validator.ValidationErrors
		if errors.As(err, &validationErr) {
			data := map[string]interface{}{
				"errors": validations.ClientInputValidaton(validationErr),
			}

			response.SendError(c, http.StatusBadRequest, "valdation error", data)
			return
		}

		response.SendError(c, http.StatusBadRequest, "bad request", err.Error())
		return
	}

	// retrieve user by the provided password reset token
	err, foundUser := authService.FindUserByPasswordResetToken(input.Token)
	if err != nil {
		msg := fmt.Sprintf("The password reset token provided has expired or is invalid")
		response.SendError(c, http.StatusBadRequest, msg, nil)
		return
	}

	// hash the password
	hashedPassword, err := auth.HashPassword(input.Password)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "password could not be hashed", err.Error())
		return
	}
	// input.Password = string(hashedPassword)

	//Set the new user password
	userDetailsToUpdate := models.User{
		Password: string(hashedPassword),
		PasswordResetToken: nil,
		PasswordResetExpires: nil,
	}
	err, updatedUser := userService.ResetPasswordResetValues(foundUser, userDetailsToUpdate)
	if err != nil {
		response.SendError(c, http.StatusInternalServerError, "failed to rreset user password", err.Error())
		return
	}

	recipientEmail := updatedUser.Email
	recipientUsername := updatedUser.Username

	from := mail.NewEmail("Zoccer Admin", os.Getenv("FROM_EMAIL"))
	subject := "Your password has been changed"
	to := mail.NewEmail(recipientUsername, recipientEmail)
	plainTextContent := ""
	htmlContent := fmt.Sprintf(`
			<span>Hello <strong>%s</strong></span>
			<br />
			<br />
			<span>
				This is a confirmation that the password for your <b>Zoccer</b> account has just been changed.
			</span>
			`, recipientUsername)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	client := email.EmailClient()
	if _, err := client.Send(message); err != nil {
		response.SendError(c, http.StatusInternalServerError, "Could not send password reset confirmation email", err.Error())
		return
	}

	response.SendSuccess(c, http.StatusOK, "Password reset successful", nil)
}
