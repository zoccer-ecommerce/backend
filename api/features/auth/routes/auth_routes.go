package routes

import (
	"github.com/gin-gonic/gin"	
	"gitlab.com/zoccer-ecommerce/backend/api/features/auth/controllers"
)


func Auth(baseApiV1 *gin.RouterGroup) *gin.RouterGroup {
	usersApi := baseApiV1.Group("/auth")
	{
		usersApi.POST("/signup/", controllers.CreateUser)
		usersApi.POST("/login/", controllers.LoginUser)
		usersApi.POST("/new-tokens/", controllers.GenerateNewAuthTokens)
		usersApi.POST("/logout/", controllers.LogoutUser)
		usersApi.POST("/forgot-password/", controllers.SendPasswordResetEmail)
		usersApi.POST("/new-password/", controllers.SetNewUserPassword)
	}

	return usersApi
}
