package response

import (
	"github.com/gin-gonic/gin"
)

type ResponseBody struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

func SendSuccess(c *gin.Context, statusCode int, message string, data interface{}) {
	status := "success"
	c.JSON(statusCode, ResponseBody{Status: status, Message: message, Data: data})
}

func SendError(c *gin.Context, statusCode int, message string, data interface{}) {
	status := "error"
	// TODO: how to return specific (and formatted) error messages
	c.JSON(statusCode, ResponseBody{Status: status, Message: message, Data: data})
}
