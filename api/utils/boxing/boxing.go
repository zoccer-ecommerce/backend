package boxing

import "time"

func BoxTime(t time.Time) *time.Time {
	return &t
}

func BoxString(s string) *string {
	return &s
}