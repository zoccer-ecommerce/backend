package pagination

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Pagination struct {
	Page     int `json:"page"`
	PageSize int `json:"pageSize"`
	// Sort     int `json:"sort"`
}

func GeneratePaginationValues(c *gin.Context) Pagination {
	// TODO: handle errors when client provides 'page' and 'pageSize' values that cannot be converted to integer
	// initialize with defaults
	pageSize := 10
	page := 1

	query := c.Request.URL.Query()

	for key, value := range query {
		queryValue := value[len(value)-1]

		switch key {
		case "page":
			page, _ = strconv.Atoi(queryValue)
			if page == 0 {
				page = 1
			}
			break
		case "pageSize":
			pageSize, _ = strconv.Atoi(queryValue)
			if pageSize > 50 { // limiting the max pageSize a client can query
				pageSize = 50
			} else if pageSize <= 0 {
				pageSize = 10
			}
			break
			// case "sort":
			// 	pageSize, _ = strconv.Atoi(queryValue)
			// 	break
		}
	}

	return Pagination{
		Page:     page,
		PageSize: pageSize,
	}
}

// GORM Scope for pagination functionality
func Paginate(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		paginationValues := GeneratePaginationValues(c)
		page := paginationValues.Page
		pageSize := paginationValues.PageSize

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
