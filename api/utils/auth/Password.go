package auth

import (
	"crypto/rand"
	"encoding/hex"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}


// Password Reset Functionality

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	bytes := make([]byte, n)
	_, err := rand.Read(bytes)
	// Note that err == nil only if we read len(bytes) bytes.
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

// Generates password reset token, converting the result to uppercase
func GeneratePasswordResetToken(n int) (string, error) {
	// generating the random bytes
	randomBytes, err := GenerateRandomBytes(n)
	if err != nil {
		return "", err
	}

	hexString := hex.EncodeToString(randomBytes)

	return strings.ToUpper(hexString), nil
}

func PasswordResetExpires() (time.Time) {
	// timastamp in milliseconds
	timeNow := time.Now().UnixNano() / 1e+6
	timeExpires := timeNow + (1 * 60 * 60 * 1000) // password expires after 1hr
	
	return time.Unix(timeExpires / 1e+3, 0)
}
