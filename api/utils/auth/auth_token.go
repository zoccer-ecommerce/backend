package auth

import (
	"errors"
	"os"

	// "strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

var accessTokenSecret = []byte(os.Getenv("ACCESS_TOKEN_SECRET"))
var refreshTokenSecret = []byte(os.Getenv("REFRESH_TOKEN_SECRET"))

// TODO: change the struct name to be more general, since the struct also includes standard claims
type JWTClaims struct {
	UserID   int    `json:"userId"`
	UserName string `json:"username"`
	IsAdmin  bool   `json:"isAdmin"`
	jwt.StandardClaims
}

type Token struct {
	Access  string `json:"accessToken"`
	Refresh string `json:"refreshToken"`
}

func GenerateAccessToken(user models.User) (string, error) {
	// hours, ok := os.LookupEnv("ACCESS_TOKEN_HOURS_VALID")
	// if !ok {
	// 	return "", errors.New("env variable ACCESS_TOKEN_HOURS_VALID is empty")
	// }
	// // convert string to int64
	// hrs, err := strconv.ParseInt(hours, 10, 64)
	// if err != nil {
	// 	return "", errors.New("could not convert env var ACCESS_TOKEN_HOURS_VALID to integer")
	// }

	myClaims := JWTClaims{
		int(user.ID),
		user.Username,
		user.IsAdmin,
		jwt.StandardClaims{
			IssuedAt: time.Now().Unix(),
			// TODO: add ExpiresAt time value to env file; reduce expiry time to 15 minutes
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(), // token expires after 1 hr
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, myClaims)

	signedToken, err := token.SignedString(accessTokenSecret)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func GenerateRefreshToken(user models.User) (string, error) {
	myClaims := JWTClaims{
		int(user.ID),
		user.Username,
		user.IsAdmin,
		jwt.StandardClaims{
			IssuedAt: time.Now().Unix(),
			// TODO: add ExpiresAt time value to env file
			ExpiresAt: time.Now().Add(time.Hour * 1 * 24 * 7).Unix(), // token expires after 7 days
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, myClaims)
	signedToken, err := token.SignedString(accessTokenSecret)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func VerifyAccessToken(tokenValue string) (*JWTClaims, error) {
	myClaims := &JWTClaims{}

	token, err := jwt.ParseWithClaims(tokenValue, myClaims, func(token *jwt.Token) (interface{}, error) {
		return accessTokenSecret, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return &JWTClaims{}, errors.New("Token is invalid or has expired")
		}
		return &JWTClaims{}, errors.New("Token is invalid or has expired")
	}

	if !token.Valid {
		return &JWTClaims{}, errors.New("Token is invalid or has expired")
	}

	return myClaims, nil
}

// TODO: how to avoid code duplication during token verification
func VerifyRefreshToken(tokenValue string) (*JWTClaims, error) {
	myClaims := &JWTClaims{}

	token, err := jwt.ParseWithClaims(tokenValue, myClaims, func(token *jwt.Token) (interface{}, error) {
		return refreshTokenSecret, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return &JWTClaims{}, errors.New("Token is invalid or has expired")
		}
		return &JWTClaims{}, errors.New("Token is invalid or has expired")
	}

	if !token.Valid {
		return &JWTClaims{}, errors.New("Token is invalid or has expired")
	}

	return myClaims, nil
}
