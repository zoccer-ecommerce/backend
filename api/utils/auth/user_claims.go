package auth

import (
	"errors"

	"github.com/gin-gonic/gin"
)

func RetrieveUserClaims(c *gin.Context) (*JWTClaims, error) {
	// Retrieving user claims from auth token
	userTokenData, exists := c.Get("userTokenData")
	if exists == false { // user token data does not exist
		return nil, errors.New("userTokenData is missing")
	}
	claims, ok := userTokenData.(*JWTClaims) // type assertion
	if !ok {                                 // type assertion not successfully
		return nil, errors.New("type asserion failed")
	}

	return claims, nil
}

