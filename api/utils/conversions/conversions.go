package conversions

// Abs returns the absolute value of an x
func Abs(x int) int {
	if x < 0 {
		return -x
	}

	return x
}