package conversions

import "testing"

func TestAbs(t *testing.T) {
	var tests = []struct {
		input    int
		expected int
	}{
		{-1, 1},
		{-5, 5},
		{0, 0},
		{23, 23},
	}

	for _, test := range tests {
		result := Abs(test.input)
		if result != test.expected {
			t.Errorf("Abs(%d) expected %d; found %d", test.input, test.expected, result)
		}
	}
}
