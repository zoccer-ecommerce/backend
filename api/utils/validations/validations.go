package validations

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

type ValidationError struct {
	Field  string `json:"field"`
	Reason string `json:"reason"`
}

func ClientInputValidaton(validationErr validator.ValidationErrors) []ValidationError {
	errs := []ValidationError{}

	for _, fieldErr := range validationErr {
		err := fieldErr.ActualTag()
		// some tags use parameters, unlke the 'required' tag
		if fieldErr.Param() != "" {
			err = fmt.Sprintf("%s=%s", err, fieldErr.Param())
		}

		errs = append(errs, ValidationError{Field: fieldErr.Field(), Reason: err})
	}

	return errs
}
