package storage

import (
	"context"
	"fmt"
	"io"
	"mime/multipart"

	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/zoccer-ecommerce/backend/config/storage"
)

// uploads image to Google Cloud Storage
func UploadImage(c *gin.Context, file multipart.File, fileHeader *multipart.FileHeader, folderName string) (string, error) {
	bucket := os.Getenv("GCLOUD_STORAGE_BUCKET")
	ctx := context.Background()
	storageClient, err := storage.StorageClient()
	if err != nil {
		return "", err
	}

	uploadedAt := time.Now().UnixNano() / 1e+6 	// timastamp in milliseconds
	fileName := strings.ReplaceAll(fileHeader.Filename, " ", "_")

	uploadPath := fmt.Sprintf("%s/%d_%s", folderName, uploadedAt, fileName)

	sw := storageClient.Bucket(bucket).Object(uploadPath).NewWriter(ctx)
	if _, err := io.Copy(sw, file); err != nil {
		return "", err
	}
	if err := sw.Close(); err != nil {
		return "", err
	}
	publicURL := "https://storage.googleapis.com/" + bucket + "/" + sw.Attrs().Name

	return publicURL, nil
}
