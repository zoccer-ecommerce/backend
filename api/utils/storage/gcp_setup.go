package storage

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Sets up Google Cloud Platform Service Account credentials
func SetupGCP(configFolder, configFilename string) error {
	// TODO: check for missing env var value
	GCPServiceAccount := os.Getenv("GCP_SERVICE_ACCOUNT")
	// fmt.Printf("\nService Account => %v\n\n", GCPServiceAccount)
	accountCredentials := []byte(GCPServiceAccount)

	// TODO: handle the error, if any
	abs, err := filepath.Abs(".")
	if err != nil {
		return err
	}
	fmt.Printf("\nAbs Path => %v\n\n", abs)
	// creating the folder
	folderPath := filepath.Join(abs, configFolder)
	fmt.Printf("\nKeys Folder Path => %v\n\n", folderPath)
	// mkdirErr := os.Mkdir(folderPath, 0644)
	mkdirErr := os.Mkdir(folderPath, 0777)
	if err != nil {
		return mkdirErr
	}
	filename := fmt.Sprintf("%s/%s", configFolder, configFilename)
	fmt.Printf("\nKeys File  => %v\n\n", filename)

	// writing to the file
	filePath := filepath.Join(abs, filename)
	fmt.Printf("\nKeys File Path => %v\n\n", filePath)
	// if err := ioutil.WriteFile(filePath, accountCredentials, 0644); err != nil {
	if err := ioutil.WriteFile(filePath, accountCredentials, 0777); err != nil {
		return err
	}

	return nil
}
