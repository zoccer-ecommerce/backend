package api

import (
	"fmt"
	"log"
	"reflect"
	"strings"

	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
	authRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/auth/routes"
	cartRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/cart/routes"
	orderRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/orders/routes"
	paymentRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/payments/routes"
	productRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/products/routes"
	userRoutes "gitlab.com/zoccer-ecommerce/backend/api/features/users/routes"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/response"
	"gitlab.com/zoccer-ecommerce/backend/api/utils/storage"

	"gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/migrations"
)

// TODO: inspect custom created functions to return errors last (and not first, as most functions do)

func Run() {
	// loading env vars (for local evelopment)
	godotenv.Load("../.env")
	// if err != nil {
	// 	// log.Fatal("Error loading .env file:", err)
	// 	fmt.Printf("\nError loading .env file: %v\n", err.Error())
	// }

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	// Setup Google Cloud Platform (GCP)
	configFolder := os.Getenv("GCLOUD_KEYS_FOLDER")
	configFilename := os.Getenv("GCLOUD_KEYS_FILENAME")
	if err := storage.SetupGCP(configFolder, configFilename); err != nil {
		log.Fatalf("\nFailed to setup GCP -> %v\n\n", err.Error())
	}

	// fmt.Printf("\nConfig Folder => %v\n\n", configFolder)
	// fmt.Printf("\nConfig Filenname => %v\n\n", configFilename)

	fmt.Printf("\nGCP Setup Successful\n\n")

	// Force log's color
	gin.ForceConsoleColor()

	// gin.SetMode(gin.ReleaseMode)
	// initializing a new gin router
	r := gin.Default()

	// gin validator customizaton
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		// using JSON tag names (omitting everything after the comma - if any) instead of field names in our validatons
		v.RegisterTagNameFunc(func(field reflect.StructField) string {
			name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]
			if name == "-" {
				return ""
			}
			return name
		})
	}

	// cors config
	corsConfig := cors.DefaultConfig()          // no origins allowed by default
	corsConfig.AllowOrigins = []string{"*"}
	// corsConfig.AllowAllOrigins = true           // TODO: proper cors corsConfig when releasing in production mode
	corsConfig.AddAllowHeaders("Authorization") // required by the angular frontend // TODO: investigate why.
	// corsConfig.AllowWebSockets = true

	r.Use(cors.New(corsConfig))

	r.Use(location.Default())

	// setting a lower memory limit for multipart forms (32MB is the default)
	r.MaxMultipartMemory = 8 << 20 // 8MB

	// database connection
	db.ConnectDatabase()

	// applying database migrations
	migrations.ApplyMigrations()

	// routes and their handlers
	r.GET("/", func(c *gin.Context) {
		response.SendSuccess(c, http.StatusOK, "Welcome to Zoccer Ecommerce APIs", nil)
	})

	baseApiV1 := r.Group("/api/v1")
	{
		baseApiV1.GET("/", func(c *gin.Context) {
			response.SendSuccess(c, http.StatusOK, "Welcome to Zoccer Ecommerce v1 APIs", nil)
		}) // api v1 welcome route

		authRoutes.Auth(baseApiV1)                 // auth routes
		userRoutes.Users(baseApiV1)                // user routes
		productRoutes.Products(baseApiV1)          // products routes
		productRoutes.ProductCategories(baseApiV1) // product-categories routes
		cartRoutes.CartItems(baseApiV1)            // cart-items routes
		orderRoutes.Orders(baseApiV1)              // orders routes
		paymentRoutes.Payments(baseApiV1)          // payments routes
	}

	// TODO: optionally configure the app's routes in an isolated package

	// start up the server
	r.Run(":" + port)
}
