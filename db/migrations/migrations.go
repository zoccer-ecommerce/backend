package migrations

import (
	setup "gitlab.com/zoccer-ecommerce/backend/db"
	"gitlab.com/zoccer-ecommerce/backend/db/models"
)

// gorm automigrations
func ApplyMigrations() {
	// TODO: explore other available advance migration options
	// initial db migration
	setup.DB.AutoMigrate(
		&models.RefreshToken{},
		&models.Product{},
		&models.ProductCategory{},
		&models.User{},
		&models.Cart{},
		&models.CartItem{},
		&models.Order{},
		&models.OrderItem{},
	)

	// other migrations not provided by AutoMigrate
}
