package db

import (
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

// TODO: look for ways to enable ssl in heroku config vars (for additional security)
func ConnectDatabase() {
	dsn := "host=" + os.Getenv("DB_HOSTNAME") +
		" port=" + os.Getenv("DB_PORT") +
		" user=" + os.Getenv("DB_USERNAME") +
		" dbname=" + os.Getenv("DB_NAME") +
		" password=" + os.Getenv("DB_PASSWORD") +
		" sslmode=" + os.Getenv("DB_SSL_MODE")

	database, err := gorm.Open(
		postgres.New(postgres.Config{
			DSN:                  dsn,
			PreferSimpleProtocol: true,
		}),
		&gorm.Config{})

	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err.Error())
	}
	// defer database.Close()

	DB = database
}
