package utils

import "time"

// embeds common fields (ID, createdAt, updatedAt) in a model struct
type CommonModelFields struct {
	ID        uint      `json:"id" gorm:"primaryKey;autoIncrement"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}
