package models

import "gitlab.com/zoccer-ecommerce/backend/db/utils"

// Order belongs to a User, UserID is the foreign key (1:1)
// Order belongs to a Cart, CartID is the foreign key (1:1)
// OrderItem has many Orders, OrderItemID is the foreign key (1:m)
type Order struct {
	utils.CommonModelFields
	CartID          uint        `json:"-"`
	UserID          uint        `json:"-"`
	User            User        `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"` // TODO: revise the constraints
	Status          string      `json:"status"`
	Subtotal        int         `json:"subTotal"`
	Discount        int         `json:"discount"`
	TotalAmount     int         `json:"totalAmount"`
	DeliveryAddress string      `json:"deliveryAddress"`
	ContactNumber   string      `json:"contactNumber"`
	// OrderItems      []OrderItem `json:"items" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

// OrderItem belongs to Order
type OrderItem struct {
	utils.CommonModelFields
	OrderID   uint    `json:"-"` //
	Order     Order   `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	ProductID uint    `json:"-"`                                                       //
	Product   Product `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"` // TODO: revise the constraints
	Status    string  `json:"status"`
}
