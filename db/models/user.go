package models

import (
	"time"

	"gitlab.com/zoccer-ecommerce/backend/db/utils"
)

type User struct {
	utils.CommonModelFields
	Username             string     `json:"username" gorm:"type:varchar(255);not null;unique"`
	Firstname            *string    `json:"firstname" gorm:"type:varchar(255)"`
	Lastname             *string    `json:"lastname" gorm:"type:varchar(255)"`
	Email                string     `json:"email" gorm:"type:varchar(255);not null;unique"`
	PhoneNumber          string     `json:"phoneNumber" gorm:"type:varchar(255);not null;unique"`
	Password             string     `json:"-" gorm:"type:varchar(255);not null"`
	Image                *string    `json:"image" gorm:"type:varchar(255)"`
	IsAdmin              bool       `json:"isAdmin" gorm:"default:false"`
	PasswordResetToken   *string    `json:"-" gorm:"type:varchar(255)"`
	PasswordResetExpires *time.Time `json:"-"`
}

type APIUser struct {
	ID          uint    `json:"id"`
	Username    string  `json:"username"`
	Firstname   *string `json:"firstname"`
	Lastname    *string `json:"lastname"`
	Email       string  `json:"email"`
	PhoneNumber string  `json:"phoneNumber"`
	Image       *string `json:"image"`
	IsAdmin     bool    `json:"isAdmin"`
}
