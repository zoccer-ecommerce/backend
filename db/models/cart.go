package models

import "gitlab.com/zoccer-ecommerce/backend/db/utils"

// Cart belongs to a User, UserID is the foreign key (1:1)
type Cart struct {
	utils.CommonModelFields
	Status string `json:"name" gorm:"size:255;not null;"`
	UserID uint   `json:"-"`
	User   User   `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Order  Order  `json:"-"` //  cart has one order
}

// CartItem belongs to Cart and Product
// CartItem has many Products, CartItemID is the foreign key (1:m)
// CartItem has many Cart, CartItemID is the foreign key (1:m) // TODO: does this make sense?
type CartItem struct {
	utils.CommonModelFields
	Quantity int  `json:"quantity"`
	CartID   uint `json:"-"` //
	Cart     Cart `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	// Carts []Cart `json:"products" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	ProductID uint    `json:"-"` //
	Product   Product `json:"product" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	// Products []Product `json:"products" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
