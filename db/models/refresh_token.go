package models

import (
	"gitlab.com/zoccer-ecommerce/backend/db/utils"
)


type RefreshToken struct {
	utils.CommonModelFields
	Token string `json:"token" gorm:"type:varchar(255)"`
}
