package models

import "gitlab.com/zoccer-ecommerce/backend/db/utils"

// ProductCategory has many Products, ProductCategoryID is the foreign key (1:m)
type ProductCategory struct {
	utils.CommonModelFields
	Name string `json:"name" gorm:"size:255;unique"`
	// Products []Product `json:"products" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

type APIProductCategory struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

// Product belongs to a ProductCategory, ProductCategoryID is the foreign key (1:1)
type Product struct {
	utils.CommonModelFields
	Name              string          `json:"name" gorm:"size:255;not null;unique"`
	Price             int             `json:"price" gorm:"not null"`
	DiscountPrice     int             `json:"discountPrice" gorm:"default:0;not null"`
	Description       string          `json:"description" gorm:"type:text"`
	Stock             int             `json:"stock" gorm:"default:0"`
	Image             *string         `json:"image" gorm:"size:255;"`
	SalesCount        int             `json:"salesCount" gorm:"default:0"`
	ProductCategoryID uint            `json:"-"`
	ProductCategory   ProductCategory `json:"category" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

type APIProduct struct {
	// utils.CommonModelFields
	ID                uint            `json:"id"`
	Name              string          `json:"name"`
	Price             int             `json:"price"`
	DiscountPrice     int             `json:"discountPrice"`
	Description       string          `json:"description"`
	Stock             int             `json:"stock"`
	Image             *string         `json:"image"`
	SalesCount        int             `json:"salesCount"`
	ProductCategory   ProductCategory `json:"category"`
}
