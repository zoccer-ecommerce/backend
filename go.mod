module gitlab.com/zoccer-ecommerce/backend

go 1.16

// +heroku goVersion go1.14

require (
	cloud.google.com/go/storage v1.14.0 // indirect
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-contrib/location v0.0.2 // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-playground/validator/v10 v10.2.0 // indirect
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.6.0 // indirect
	github.com/sendgrid/rest v2.6.3+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.8.0+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b // indirect
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/api v0.40.0 // indirect
	gorm.io/driver/postgres v1.0.8 // indirect
	gorm.io/gorm v1.21.3 // indirect
)
