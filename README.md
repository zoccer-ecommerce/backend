# Zoccer Ecommerce REST APIs
<!-- Simple overview of use/purpose. -->
A backend application providing RESTful APIs for Zoccer Ecommerce platforms
## Description
<!-- An in-depth paragraph about your project and overview of use. -->
This is a Golang backend application that provides RESTful APIs for a company (Zoccer) trading in sports equipment. The Payments section is handled by Zoccer Ecommerce Microservice ([go to repo](https://gitlab.com/zoccer-ecommerce/backend-microservices/payments.git))
## Getting Started
### Dependencies
<!-- * Describe any prerequisites, libraries, OS version, etc., needed before installing program.
* ex. Windows 10 -->
* [Golang](https://golang.org/dl/) (v1.16 preferred)
* [SendGrid](https://app.sendgrid.com/) - create an account and generate an API key
* [Google Cloud Storage](https://cloud.google.com/storage/) - create an account, add a project, create a storage bucket then download the created project's service account key to your machine
### Installing
<!-- * How/where to download your program
* Any modifications needed to be made to files/folders -->
* clone this [repository](https://gitlab.com/zoccer-ecommerce/backend.git) to your machine
* run the command `go get -u ./..` at the root directory of your project to download and install the go package dependencies specified in the `go.mod` file
* create `.env` file at the root of your project and populate it using the template below. (NOTE: use your own values instead of the values with square brackets):
```
PORT=[your-development-server-port-number]

DB_HOSTNAME=[your-database-host-name]
DB_PORT=[your-database-port-number]
DB_USERNAME=[your-database-username]
DB_NAME=[your-database-name]
DB_PASSWORD=[your-database-password]
DB_SSL_MODE=[your-database-sslmode]

ACCESS_TOKEN_SECRET=[your-access-token-secret]
REFRESH_TOKEN_SECRET=[your-refresh-token-secret]
ACCESS_TOKEN_HOURS_VALID=[your-access-token-validity-period-in-hours]
REFRESH_TOKEN_HOURS_VALID=[your-refresh-token-validity-period-in-hours]

SENDGRID_API_KEY=[your-sendgrid-api-key]
FROM_EMAIL=[email-to-display-when-sending-emails]

GCLOUD_STORAGE_BUCKET=[your-google-cloud-storage-bucket-name]
GCLOUD_KEYS_FOLDER=../config/googleCloudStorage
GCLOUD_KEYS_FILENAME=keys.json
GCP_SERVICE_ACCOUNT=[google-cloud-service-account-json-object]

CLIENT_HOST=[your-frontend-client-host-url]

PAYPAL_BASE_API=[your-paypal-microservice-base-url-here]

```
### Executing program
<!-- * How to run the program
* Step-by-step bullets
```
code blocks for commands
``` -->
* run the command below at the root directory of your project. This builds the go project with the name `backend` at `bin` directory and finally executes the built project
```
go build -o bin/backend -v && cd bin && ./backend
```
### Running Tests
#### Example:
##### Running tests recursively all packages
```go test -v ./...```
##### Running all tests in utils package
```go test -v ./api/utils/...```
<!-- ## Help
Any advise for common problems or issues.
```
command to run if program contains helper info
``` -->
## Authors
<!-- Contributors names and contact info
ex. Dominique Pizzie  
ex. [@DomPizzie](https://twitter.com/dompizzie) -->
* Moses Adongo ([Gitlab Repo](https://gitlab.com/AdongoJr))
<!-- ## Version History
* 0.2
* Various bug fixes and optimizations
* See [commit change]() or See [release history]()
* 0.1
* Initial Release -->
<!-- ## License
This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details -->
## Acknowledgments
Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
<!-- * [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [dbader](https://github.com/dbader/readme-template)
* [zenorocha](https://gist.github.com/zenorocha/4526327)
* [fvcproductions](https://gist.github.com/fvcproductions/1bfc2d4aecb01a834b46) -->