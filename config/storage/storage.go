package storage

import (
	"context"
	"fmt"
	"os"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

// storage configuration
func StorageClient() (*storage.Client, error) {
	var storageClient *storage.Client

	ctx := context.Background()
	storageCredentialsFile := fmt.Sprintf("%s/%s", os.Getenv("GCLOUD_KEYS_FOLDER"), os.Getenv("GCLOUD_KEYS_FILENAME"))

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile(storageCredentialsFile))
	if err != nil {
		return storageClient, nil
	}

	return storageClient, nil
}