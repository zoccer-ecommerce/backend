package email

import (
	"os"

	"github.com/sendgrid/sendgrid-go"
)

// Email configuration
func EmailClient() *sendgrid.Client {
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))

	return client
}
