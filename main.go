package main

import "gitlab.com/zoccer-ecommerce/backend/api"

func main() {
	api.Run()
}